# Assignment

The problem to solve is following:

                You monitor devices, which are sending data to you.

                Each device has a unique name.

                Each device produces measurements.

The challenge is:

                Compute number of messages you got or read from the devices.

The solution should be written in C++.

The scope is open, you must decide how the “devices” will work in your system.

The solution should be posted on GitHub or a similar page for a review.

Please add documentation explaining us how to run your code.

# Build instructions

Prerequisites:
 - *cmake* (tested on 3.23.1)
 - *gcc*  (tested on 11)
 - Any Linux operating system

To build the release version (e.g. in _build_ directory):

```shell
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make all
```

# Launch instructions

Two targets are in the build directory after a build:
 - *device-srv* : a server part
 - *device-cli* : a client part which represents a ``device''

The server passively listens on arriving UDP datagrams on a given port.
The datagrams are expected to contain an id within the first 4 bytes, and the
rest is understood as raw data (e.g. measurement results). Server counts the
 amount of arriving messages per each id, and constantly prints the gathered
 stats on stdout.

Each client instance (_device_) will continuously send 4 bytes of id and
4 bytes of ``measured data'' within a transaction.

Start server like follows:

```shell
./device-srv -p 12345
```

Start as many clients instances as you like as follows:

```
./device-cli -h localhost -p 12345
```

Additionally, a client application can take an id with an `-i` argument. If `-i`
isn't given, id is generated randomly when the app starts.
