#ifndef DEVICE_CLIENT_SERVER_SRC_CLI_CLIENT_H_
#define DEVICE_CLIENT_SERVER_SRC_CLI_CLIENT_H_

#include <string>
#include <vector>

#include <netinet/in.h>

namespace cli {

const size_t MAX_BUFFER_SIZE = 256;

class Client {
 public:
  Client();
  ~Client();

  bool setup(const std::string &host, int port);
  bool send(const std::vector<uint8_t> &data);

 private:
  int _skfd;
  struct sockaddr_in _dest_addr;
};

}
#endif //DEVICE_CLIENT_SERVER_SRC_CLI_CLIENT_H_
