#include <iostream>
#include <thread>
#include <unordered_map>

#include <getopt.h>
#include <csignal>

#include "srv/Server.h"
#include "Message.h"

using namespace srv;
using namespace std;
using namespace std::chrono;

struct ProgramConfig {
  int port;
  static constexpr int MANDATORY_ARGN = 3;
  static constexpr char OPTSTR[] = "p:";

  static void printHelp() {
    cout << "-p [PORT] : a port to listen at." << endl;
  }

  bool fillFromArgs(int argc, char *const argv[]) {
    optind = 0;
    if (argc < MANDATORY_ARGN) {
      cerr << "Not enough arguments." << endl;
      return false;
    }
    int opt;
    while ((opt = getopt(argc, argv, OPTSTR)) != -1) {
      if ('p' == opt)
        stringstream(optarg) >> port;
      else
        return false;
    }
    return true;
  }

} config;

static bool run = true;
static void signal_handler(int sig) {
  if (sig == SIGTERM || sig == SIGINT)
    run = false;
}

int main(int argc, char *const argv[]) {
  signal(SIGTERM, signal_handler);
  signal(SIGINT, signal_handler);

  if (!config.fillFromArgs(argc, argv)) {
    config.printHelp();
    return EXIT_FAILURE;
  }

  Server srv;
  srv.start(config.port);

  unordered_map<uint32_t, pair<size_t, size_t>> perIdStat;
  atomic<bool> updatedStat = false;

  auto printStat = [&perIdStat]() {
    cout << flush << "----------" << endl;
    for (const auto &[id, stat] : perIdStat) {
      cout << hex << id << dec << ": " << "Mgs = " << stat.first << ", Measured = " << stat.second
           << " bytes" << endl;
    }
  };

  auto handleMessage = [&perIdStat, &updatedStat](const vector<uint8_t> &data) {
    Message msg(data);
    if (!perIdStat.count(msg.id())) perIdStat[msg.id()] = {0, 0};
    perIdStat[msg.id()].first++;
    perIdStat[msg.id()].second += msg.sizeNoId();
    updatedStat = true;
  };

  srv.registerOnReceiveMsgCb(handleMessage);

  for (run = true; run; this_thread::sleep_for(150ms)) {
    if (updatedStat.load()) {
      printStat();
      updatedStat = false;
    }
  }

  cout << "Final Statistics:" << endl;
  printStat();

  return 0;
}
