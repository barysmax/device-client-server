#include <iostream>
#include <thread>
#include <chrono>
#include <sstream>

#include <getopt.h>
#include <csignal>

#include "cli/Client.h"
#include "Message.h"

using namespace cli;
using namespace std;
using namespace std::chrono;

struct ProgramConfig {
  int port = -1;
  uint32_t id = 0;
  string hostname;

  static void printHelp() {
    cout << "-H [hostname]\t: a hostname for sending." << endl;
    cout << "-p [PORT]\t: a port for sending." << endl;
    cout << "-i id\t: an id of the client, if not set, random is used." << endl;
  }

  bool fillFromArgs(int argc, char *const argv[]) {
    optind = 0;
    int opt;
    while ((opt = getopt(argc, argv, "p:H:i:")) != -1) {
      if ('p' == opt)
        stringstream(optarg) >> port;
      else if ('H' == opt)
        stringstream(optarg) >> hostname;
      else if ('i' == opt)
        stringstream(optarg) >> id;
      else
        return false;
    }
    if (port == -1) {
      cout << "Port is mandatory!" << endl;
      return false;
    }

    if (hostname.empty()) {
      cout << "Hostname is mandatory!" << endl;
      return false;
    }

    if (id == 0) {
      id = rand() % UINT32_MAX;
    }
    return true;
  }

} config;

static bool run = true;
static void signal_handler(int sig) {
  if (sig == SIGTERM || sig == SIGINT)
    run = false;
}

int main(int argc, char *const argv[]) {
  signal(SIGTERM, signal_handler);
  signal(SIGINT, signal_handler);
  srand(time(NULL));

  if (!config.fillFromArgs(argc, argv)) {
    config.printHelp();
    return EXIT_FAILURE;
  }

  cout << "CLI with id [" << std::hex << config.id << std::dec << "] reports to " << config.hostname
       << ":" << config.port << endl;
  cout.flush();

  Client cli;

  auto getMeasurements = []() -> vector<uint8_t> {
    return {static_cast<uint8_t>(rand() % UINT8_MAX), static_cast<uint8_t>(rand() % UINT8_MAX),
            static_cast<uint8_t>(rand() % UINT8_MAX), static_cast<uint8_t>(rand() % UINT8_MAX)};
  };

  long long int countMsgsSent = 0;
  long long int countMeasBytesSent = 0;

  auto printStat = [&]() {
    cout << hex << config.id << dec << ": " << "Mgs = " << countMsgsSent << ", Measured = "
         << countMeasBytesSent << " bytes" << endl;
  };

  bool connected = false;
  for (; run; this_thread::sleep_for(50ms)) {
    auto buf = getMeasurements();
    if (!connected) connected = cli.setup(config.hostname, config.port);
    if (connected) {
      Message msg(config.id, buf);
      if (cli.send(msg.data())) {
        countMeasBytesSent += buf.size();
        countMsgsSent++;
        printStat();
      } else {
        connected = false;
      }
    }
  }

  printStat();

  return EXIT_SUCCESS;
}
