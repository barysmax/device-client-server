#ifndef DEVICE_CLIENT_SERVER_SERVER_H
#define DEVICE_CLIENT_SERVER_SERVER_H

#include <string>
#include <functional>
#include <future>
#include <vector>
#include <mutex>
#include <netinet/in.h>

namespace srv {
using OnReceiveCallback = std::function<void(std::vector<uint8_t> data)>;

const size_t MAX_BUFFER_SIZE = 256;
const int POLL_TIMEOUT_MS = 10;

class Server {
 public:
  Server();

  bool start(unsigned int port);
  void stop();
  void registerOnReceiveMsgCb(OnReceiveCallback cb);

  ~Server();
 private:
  std::future<void> _work;
  std::atomic<bool> _run{false};
  OnReceiveCallback _cb;
  std::mutex _cbMx;
  int _skfd = -1;
  struct sockaddr_in _serv_addr;
};
}
#endif //DEVICE_CLIENT_SERVER_SERVER_H
