#include "srv/Server.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/poll.h>

#include <iostream>
#include <chrono>
#include <cstring>
#include <thread>

using namespace srv;
using namespace std;
using namespace std::chrono;

Server::Server() {
  _skfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (_skfd < 0)
    cerr << "Can't get socket fd: " << strerror(errno) << endl;
  bzero(reinterpret_cast<char *>(&_serv_addr), sizeof(_serv_addr));
  _serv_addr.sin_addr.s_addr = INADDR_ANY;
}

bool Server::start(unsigned int port) {
  if (_work.valid()) {
    cerr << "Stop the server first." << endl;
    return false;
  }

  _serv_addr.sin_port = htons(port);
  if (bind(_skfd, reinterpret_cast<const sockaddr *>(&_serv_addr), sizeof(_serv_addr))) {
    cerr << "Can't bind: " << strerror(errno) << endl;
    return false;
  }

  _work = std::async([&]() {
    pollfd pfd = {.fd = _skfd, .events = POLLIN, .revents = 0};
    for (_run = true; _run;) {
      int poll_ret = poll(&pfd, 1, POLL_TIMEOUT_MS);
      if (poll_ret > 0 && (pfd.revents & POLLIN)) {
        sockaddr_in src_addr;
        socklen_t addrlen;
        uint8_t buf[MAX_BUFFER_SIZE];
        auto nbytes = recvfrom(_skfd,
                               buf,
                               MAX_BUFFER_SIZE,
                               0,
                               reinterpret_cast<sockaddr *>(&src_addr),
                               &addrlen);
        if (nbytes < 0) { // UDP permits zero length datagrams
          cerr << "recvfrom(): " << strerror(errno) << endl;
          continue;
        }
#ifdef DEBUG
        cout << inet_ntoa(src_addr.sin_addr) << ":" << ntohs(src_addr.sin_port) << " sent "
             << nbytes << " bytes." << endl;
#endif
        {
          lock_guard lk(_cbMx);
          if (_cb) {
            _cb({buf, buf + nbytes});
          } else {
            cerr << "Server: no callback registered! Drop the message." << endl;
          }
        }
      }
    }
  });
  return true;
}

void Server::stop() {
  _run = false;
  if (_work.valid())
    _work.wait();
}

void srv::Server::registerOnReceiveMsgCb(srv::OnReceiveCallback cb) {
  lock_guard lk(_cbMx);
  _cb = move(cb);
}

Server::~Server() {
  stop();
  close(_skfd);
}

