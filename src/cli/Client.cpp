#include "cli/Client.h"

#include <iostream>
#include <cstring>
#include <cerrno>

#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>

using namespace cli;
using namespace std;

Client::Client() {
  _skfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (_skfd < 0)
    cerr << "Can't get socket fd: " << strerror(errno) << endl;
}

Client::~Client() {
  close(_skfd);
}

bool Client::setup(const std::string &host, int port) {
  addrinfo *host_addr_info;
  addrinfo
      hints = {.ai_flags = 0, .ai_family = AF_INET, .ai_socktype = SOCK_DGRAM, .ai_protocol = 0};
  if (int err = getaddrinfo(host.c_str(), to_string(port).c_str(), &hints, &host_addr_info)) {
    cerr << "getaddrinfo(): " << gai_strerror(err) << endl;
    return false;
  }
  bcopy(host_addr_info->ai_addr, &_dest_addr, host_addr_info->ai_addrlen); // take the first one
  freeaddrinfo(host_addr_info);
  return true;
}

bool Client::send(const std::vector<uint8_t> &data) {
  for (auto bufStart = data.begin(); bufStart < data.end();) {
    uint8_t buf[MAX_BUFFER_SIZE];
    auto bufEnd = std::min((bufStart + MAX_BUFFER_SIZE), data.end());
    std::copy(bufStart, bufEnd, buf);
    size_t bufSize = bufEnd - bufStart;
    size_t nbytes = sendto(_skfd,
                           buf,
                           bufSize,
                           0,
                           reinterpret_cast<const sockaddr *>(&_dest_addr),
                           sizeof(_dest_addr));
    if (nbytes < 0) {
      cerr << "Can't send: " << strerror(errno);
      return false;
    }
    if (nbytes < bufSize) {
      bufStart += nbytes;
    } else {
      bufStart += MAX_BUFFER_SIZE;
    }
  }
  return true;
}

