#ifndef DEVICE_CLIENT_SERVER_INCLUDE_MESSAGE_H_
#define DEVICE_CLIENT_SERVER_INCLUDE_MESSAGE_H_

#include <arpa/inet.h>
#include <vector>
#include <sstream>
#include <ostream>
#include <iomanip>

class Message {
 public:
  Message() : _data{0, 0, 0, 0}, _id(0) {};

  explicit Message(const std::vector<uint8_t> &data) : _data(data) {
    _id = ntohl(static_cast<uint32_t>(data[0] & 0xFF) | static_cast<uint32_t>((data[1] << 8))
                    | static_cast<uint32_t>((data[2] << 16))
                    | static_cast<uint32_t>((data[3] << 24)));
  }

  Message(uint32_t id, const std::vector<uint8_t> &measurements) : _id(id) {
    uint32_t networkOrder = htonl(id);
    _data.emplace_back(static_cast<uint8_t>(networkOrder & 0xFF));
    _data.emplace_back(static_cast<uint8_t>((networkOrder >> 8) & 0xFF));
    _data.emplace_back(static_cast<uint8_t>((networkOrder >> 16) & 0xFF));
    _data.emplace_back(static_cast<uint8_t>((networkOrder >> 24) & 0xFF));
    _data.insert(_data.end(), measurements.cbegin(), measurements.cend());
  };

  [[nodiscard]] const std::vector<uint8_t> &data() const { return _data; };

  [[nodiscard]] uint32_t id() const { return _id; };
  [[nodiscard]] size_t sizeNoId() const { return _data.size() - sizeof(_id); };

  friend std::ostream &operator<<(std::ostream &os, const Message &message) {
    std::stringstream ss;
    ss << std::hex << message._id;
    os << " id: 0x" << ss.str();

    auto printHexMsg = [](const std::vector<uint8_t> &data) -> std::string {
      std::stringstream ss;
      for (auto byte : data) {
        ss << " " << std::setfill('0') << std::setw(2) << std::hex << static_cast<int>(byte);
      }
      return ss.str();
    };

    os << " [" << printHexMsg(message._data) << "]" << std::endl;
    return os;
  }
 private:
  std::vector<uint8_t> _data;
  uint32_t _id;
};

#endif //DEVICE_CLIENT_SERVER_INCLUDE_MESSAGE_H_